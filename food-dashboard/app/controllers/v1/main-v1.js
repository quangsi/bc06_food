import { MonAn } from "../../models/v1/model-v1.js";
import showInfo, { layThongTinTuForm } from "./controller-v1.js";

let themMon = () => {
  let data = layThongTinTuForm();
  console.log(`  🚀: themMon -> data`, data);
  // tạo object từ class
  let { ma, ten, loai, gia, khuyenMai, tinhTrang, hinhMon, moTa } = data;
  let monAn = new MonAn(
    ten,
    ma,
    loai,
    gia,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  showInfo(monAn);
};

document.getElementById("btnThemMon").addEventListener("click", themMon);
