export let renderFoodList = (foodArr) => {
  let contentHTML = "";
  foodArr.forEach((item) => {
    let contentTr = `
 <tr>
 <td>${item.id}</td>
 <td>${item.name}</td>
 <td>${item.type ? "Chay" : "Mặn"}</td>
 <td>${item.price}</td>
 <td>${item.discount}</td>
 <td>0</td>
 <td>${item.status ? "Còn" : "Hết"}</td>
 <td>
 <button class="btn btn-danger" onclick="deleteFood(${item.id})">Xoá</button>

  <button class="btn btn-success" onclick="editFood(${item.id})">Sửa</button>
 </td>
 </tr>
 `;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
export let onSuccess = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "right", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};

/**
 * {
    "name": "Dynamic Brand Liaison",
    "type": true,
    "discount": 79358,
    "img": "http://loremflickr.com/640/480/food",
    "desc": "20362-9695",
    "price": 74349,
    "status": true,
    "id": "21"
}
 */

let isLogin = true;

let thongBao = isLogin ? "Success" : "Fail";
