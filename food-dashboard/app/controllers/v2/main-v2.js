import { layThongTinTuForm } from "../v1/controller-v1.js";
import { onSuccess, renderFoodList } from "./controller-v2.js";
// true => chay, còn

const BASE_URl = "https://633ec05b0dbc3309f3bc5455.mockapi.io";

// lấy dữ liệu từ server

let fetchFoodList = () => {
  axios({
    url: `${BASE_URl}/food`,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      renderFoodList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchFoodList();

let deleteFood = (id) => {
  axios({
    url: `${BASE_URl}/food/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      onSuccess("Xoá thành công");
      fetchFoodList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deleteFood = deleteFood;

let createFood = () => {
  let data = layThongTinTuForm();
  axios({
    url: `${BASE_URl}/food`,
    method: "POST",
    // data: data,
    data,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      onSuccess("Thêm thành công");
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.createFood = createFood;

window.editFood = () => {
  console.log("yes");
  $("#exampleModal").modal("show");
};
